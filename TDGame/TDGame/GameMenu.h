#include "Core.h"
#include "Menu.h"
#pragma once
class GameMenu : public Menu
{
public:
	GameMenu(void);
	~GameMenu(void);
	sf::Texture tDirt;
	sf::Texture tWall;
	sf::Texture tGrass;
	sf::Texture tWall2;
	sf::Texture tHeli;
	sf::Texture tBase;

	sf::Texture iBackground;
	sf::Sprite sBackground;

	sf::Sprite bg;
	sf::Sprite bg_base;

	sf::RectangleShape grid;

	sf::Texture tMenuTower;
	sf::Texture tMenuGun;
	sf::Texture tMenuEnemy;
	sf::Texture tMenuTank;

	sf::Sprite sMenuTower;
	sf::Sprite sMenuGun;
	sf::Sprite sMenuEnemy;
	sf::Sprite sMenuTank;

	sf::Text MONEY_TEXT, MONEY_counter;
	sf::Text KILLED_TEXT, KILLED_counter;
	sf::Text BASEHP_TEXT, BASEHP_counter;

	sf::RectangleShape StalinCube;
	sf::Text StalinSay;

	bool clickTower, clickGun;
	bool clickEnemy, clickTank;

	//���������� ������
	int killed_counter; //������� �������
	int money;
	int baseHP; //����� ����, ���-�� ������ �� ���������

	bool waveClick;
	int i;

	std::wstring strStalin;

	void pressMenuTower(bool clickTower, int num_tower, int money, sf::Text counter);
	void pressMenuTower(char towerType);
	void pressMenuEnemy(char enemyType);

	int num_tower, num_gun;
	int num_enemy, num_tank;

	float frame;

	float wave;

	float EnemyV_currentFrame; //�������� ���� ������� �����
	float TankV_currentFrame; //�������� ���� ������� �����
	float TowerV_currentFrame; //�������� �������� �����
	float GunV_currentFrame; //�������� �������� ���������

	void renderTileMap(sf::String TileTagMap, int i, int j);

	void killed(char enemyType);

	float enemyOnBase(void);

	void drawTileMap(sf::RenderWindow &window);

	void enemiesWaves();

	void clickingTowers(sf::RenderWindow &window);

	void clickingEnemy(sf::RenderWindow &window);

	void drawGame(sf::RenderWindow &window);
};

