#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Core.h"

#pragma once

class MainMenu : public Core
{
public:
	MainMenu(void);
	~MainMenu(void);
	sf::RectangleShape start, options, exit;
	sf::Text START_TEXT, OPT_TEXT, EXIT_TEXT, LOADING_TEXT;
	sf::Text ABOUT_TEXT;
	std::wstring strAbout_text;
	sf::Music Menu_Theme;
	sf::SoundBuffer ClickBuffer;
	sf::Sound sClick;
	sf::Texture iBackground;
	sf::Sprite sBackground;
	void returnDefaultColors();
	int getMenuPos(sf::RenderWindow &window);
	void drawMenu(sf::RenderWindow &window);
};
