#pragma once

#include "GameMenu.h"
#include "Tower.h"
#include "Soldier.h"
#include "Tank.h"

class Laser : public Tower
{
public:
	Laser(void);
	~Laser(void);
	bool secondClick();
	void laserAttackSoldier(GameMenu &GameMenu, Soldier * soldier_arr, int tX, int tY, float frame);
	bool laserAttackTank(GameMenu &GameMenu, Tank * tank_arr, int tX, int tY, float frame);
	void setLaser(GameMenu &GameMenu, sf::RenderWindow &window);
};