#include "MainMenu.h"
#include <string>

MainMenu::MainMenu(void)
{
	iBackground.loadFromFile("Res/Redbanner.jpg");
	sBackground.setTexture(iBackground);

	start.setSize(sf::Vector2f(100, 50));
	start.setOutlineColor(sf::Color::Black);
	start.setFillColor(sf::Color::Yellow);
	start.setOutlineThickness(5);
	start.setPosition(windowWidth * 8/10, windowHeight * 7/10);

	options.setSize(sf::Vector2f(130, 50));
	options.setOutlineColor(sf::Color::Black);
	options.setFillColor(sf::Color::Yellow);
	options.setOutlineThickness(5);
	options.setPosition(windowWidth * 8/10, windowHeight * 8/10);

	exit.setSize(sf::Vector2f(100, 50));
	exit.setOutlineColor(sf::Color::Black);
	exit.setFillColor(sf::Color::Yellow);
	exit.setOutlineThickness(5);
	exit.setPosition(windowWidth * 8/10, windowHeight * 9/10);

	Menu_Theme.openFromFile("Res/Menu_Theme.ogg");
	ClickBuffer.loadFromFile("Res/click.wav");
	sClick.setBuffer(ClickBuffer);

	START_TEXT.setString("Start");
	START_TEXT.setFont(font);
	START_TEXT.setColor(sf::Color::Black);
	START_TEXT.setCharacterSize(fontSize);
	START_TEXT.setPosition(windowWidth * 8/10, windowHeight * 7/10);

	OPT_TEXT.setString("About");
	OPT_TEXT.setFont(font);
	OPT_TEXT.setColor(sf::Color::Black);
	OPT_TEXT.setCharacterSize(fontSize);
	OPT_TEXT.setPosition(windowWidth * 8/10, windowHeight * 8/10);

	EXIT_TEXT.setString("Exit");
	EXIT_TEXT.setFont(font);
	EXIT_TEXT.setColor(sf::Color::Black);
	EXIT_TEXT.setCharacterSize(fontSize);
	EXIT_TEXT.setPosition(windowWidth * 8/10 + 15, windowHeight * 9/10);

	LOADING_TEXT.setString("Loading...");
	LOADING_TEXT.setFont(font);
	LOADING_TEXT.setColor(sf::Color::Black);
	LOADING_TEXT.setCharacterSize(fontSize);
	LOADING_TEXT.setPosition(windowWidth / 2 - 63, windowHeight * 9 / 10);

	strAbout_text = L"���� ������\n���������� �.�.\n�� ������ ���-219";
	ABOUT_TEXT.setString(strAbout_text);
	ABOUT_TEXT.setFont(Sfont);
	ABOUT_TEXT.setColor(sf::Color::Black);
	ABOUT_TEXT.setCharacterSize(fontSize);
	ABOUT_TEXT.setPosition(windowWidth / 2 - 63, windowHeight * 6 / 10);
}

MainMenu::~MainMenu(void)
{
}

void MainMenu::returnDefaultColors()
{
	MainMenu::start.setOutlineColor(sf::Color::Black);
	MainMenu::options.setOutlineColor(sf::Color::Black);
	MainMenu::exit.setOutlineColor(sf::Color::Black);
}

int MainMenu::getMenuPos(sf::RenderWindow &window)
{
	//�������� ������� ����
	if (sf::IntRect(MainMenu::start.getPosition().x, MainMenu::start.getPosition().y, 100, 50).contains(sf::Mouse::getPosition(window))) {MainMenu::start.setOutlineColor(sf::Color::Red); return 1;}
	if (sf::IntRect(MainMenu::options.getPosition().x, MainMenu::options.getPosition().y, 130, 50).contains(sf::Mouse::getPosition(window))) {MainMenu::options.setOutlineColor(sf::Color::Red); return 2;}
	if (sf::IntRect(MainMenu::exit.getPosition().x, MainMenu::exit.getPosition().y, 100, 50).contains(sf::Mouse::getPosition(window))) {MainMenu::exit.setOutlineColor(sf::Color::Red); return 3;}
}

void MainMenu::drawMenu(sf::RenderWindow &window)
{
	window.draw(MainMenu::sBackground);
	window.draw(MainMenu::start); //���������(����� ���� ���������)
	window.draw(MainMenu::START_TEXT);
	window.draw(MainMenu::options);
	window.draw(MainMenu::OPT_TEXT);
	window.draw(MainMenu::exit);
	window.draw(MainMenu::EXIT_TEXT);
}