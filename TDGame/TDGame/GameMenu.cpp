#include "GameMenu.h"

GameMenu::GameMenu(void)
{
	tDirt.loadFromFile("Res/bg_dirt.png");

	tWall.loadFromFile("Res/object_1_grey.png");

	tGrass.loadFromFile("Res/bg_grass.png");

	tWall2.loadFromFile("Res/bg_wall.png");

	tHeli.loadFromFile("Res/Heli.png");

	tBase.loadFromFile("Res/Base1.png");

	bg.setScale(cellSize / 32, cellSize / 32); //�������� ������ ���� ������ � �� ���� ����������� ��� ���������

	bg_base.setScale(cellSize / 64, cellSize / 64);

	iBackground.loadFromFile("Res/wallGame.jpg");
	sBackground.setTexture(iBackground);

	num_tower = 0; // ���-�� �����
	num_gun = 0; //���-�� ���������
	num_enemy = 0; // ���������� ������
	num_tank = 0;

	killed_counter = 0;
	baseHP = 5; // ���� ����������� ������ � ����� ����
	money = 100500;

	clickTower = false;  //�������� ������� �����
	clickGun = false;
	clickEnemy = false;
	clickTank = false;

	waveClick = false;
	i = 0;

	frame = 500;
	wave = 0;
	EnemyV_currentFrame = 0;
	TankV_currentFrame = 0;
	TowerV_currentFrame = 0;
	GunV_currentFrame = 0;

	MONEY_TEXT.setString("Money:");
	MONEY_TEXT.setFont(font);
	MONEY_TEXT.setColor(sf::Color::Black);
	MONEY_TEXT.setCharacterSize(fontSize);
	MONEY_TEXT.setPosition(windowWidth * 3/4 - fontSize * 4, windowHeight / 8);

	MONEY_counter.setString(std::to_string(money));
	MONEY_counter.setFont(font);
	MONEY_counter.setColor(sf::Color::Black);
	MONEY_counter.setCharacterSize(fontSize);
	MONEY_counter.setPosition(windowWidth * 3/4, windowHeight / 8);

	KILLED_TEXT.setString("Killed:");
	KILLED_TEXT.setFont(font);
	KILLED_TEXT.setColor(sf::Color::Black);
	KILLED_TEXT.setCharacterSize(fontSize);
	KILLED_TEXT.setPosition(windowWidth * 16/20 - fontSize * 4, windowHeight * 2.25 / 8);

	KILLED_counter.setString(std::to_string(killed_counter));
	KILLED_counter.setFont(font);
	KILLED_counter.setColor(sf::Color::Black);
	KILLED_counter.setCharacterSize(fontSize);
	KILLED_counter.setPosition(windowWidth * 16/20, windowHeight * 4.5 / 16);

	BASEHP_TEXT.setString("Base HP:");
	BASEHP_TEXT.setFont(font);
	BASEHP_TEXT.setColor(sf::Color::Black);
	BASEHP_TEXT.setCharacterSize(fontSize);
	BASEHP_TEXT.setPosition(windowWidth * 16/20 - fontSize * 5, windowHeight * 6 / 16);

	BASEHP_counter.setString(std::to_string(baseHP));
	BASEHP_counter.setFont(font);
	BASEHP_counter.setColor(sf::Color::Black);
	BASEHP_counter.setCharacterSize(fontSize);
	BASEHP_counter.setPosition(windowWidth * 16/20, windowHeight * 6 / 16);

	strStalin = L"������ \n ������";
	StalinSay.setString(strStalin);
	StalinSay.setFont(Sfont);
	StalinSay.setColor(sf::Color::Yellow);
	StalinSay.setCharacterSize(fontSize);
	StalinSay.setPosition(windowWidth * 6/10, windowHeight * 8.5/10);

	grid.setSize(sf::Vector2f(Core::cellSize, Core::cellSize));
	grid.setFillColor(sf::Color::Transparent);
	grid.setOutlineColor(sf::Color(102,102,102));
	grid.setOutlineThickness(1);

	tMenuTower.loadFromFile("Res/MenuTower.png");
	tMenuTower.setSmooth(true);

	tMenuGun.loadFromFile("Res/tank3_base_red.png");
	tMenuGun.setSmooth(true);

	tMenuEnemy.loadFromFile("Res/soldier_armcannon_grey.png");
	tMenuEnemy.setSmooth(true);

	tMenuTank.loadFromFile("Res/tank.png");
	tMenuTank.setSmooth(true);

	sMenuTower.setTexture(tMenuTower);
	sMenuTower.setScale(cellSize / 32, cellSize / 32);
	sMenuTower.setPosition(108, 635);

	sMenuGun.setTexture(tMenuGun);
	sMenuGun.setScale(cellSize / 32, cellSize / 32);
	sMenuGun.setPosition(12, 635);

	sMenuEnemy.setTexture(tMenuEnemy);
	sMenuEnemy.setOrigin(tMenuEnemy.getSize().x - cellSize / 2, tMenuEnemy.getSize().y - cellSize / 2);
	sMenuEnemy.setScale(cellSize / 32, cellSize / 32);
	sMenuEnemy.setPosition(windowWidth / 5, windowHeight * 5 / 6);

	sMenuTank.setTexture(tMenuTank);
	sMenuTank.setOrigin(tMenuEnemy.getSize().x - cellSize / 2, tMenuEnemy.getSize().y - cellSize / 2);
	sMenuTank.setScale(cellSize / 32, cellSize / 32);
	sMenuTank.setPosition(302, 634);
}

GameMenu::~GameMenu(void)
{
}

void GameMenu::pressMenuTower(char towerType)
{
	switch (towerType)
	{
	case 'L': //����� �����
		GameMenu::clickTower = true;
		GameMenu::num_tower++; //+1 ����� �������
		GameMenu::money -= 50;
		break;
	case 'G': //����� �����
		GameMenu::clickGun = true;
		GameMenu::num_gun++; //+1 ����� �������
		GameMenu::money -= 100;
		break;
	default:
		break;
	}
	GameMenu::MONEY_counter.setString(std::to_string(GameMenu::money));
}

void GameMenu::pressMenuEnemy(char enemyType)
{
	switch (enemyType)
	{
	case 'S': //����� ������
		GameMenu::clickEnemy = true;
		GameMenu::num_enemy++;
		break;
	case 'T': //����� ����
		GameMenu::clickTank = true;
		GameMenu::num_tank++;
		break;
	default:
		break;
	}
	sf::sleep(sf::milliseconds(150));
}

void GameMenu::renderTileMap(sf::String TileTagMap, int i, int j)
{
	// ���� - bg_dirt, ����� ��� ����������� ����� - bg_wall
	if (TileTagMap == 'B') { GameMenu::bg.setTexture(tWall2); GameMenu::bg.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); }

	if (TileTagMap == 'S') { GameMenu::bg.setTexture(tDirt); GameMenu::bg.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); GameMenu::bg_base.setTexture(tHeli); GameMenu::bg_base.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); }

	if (TileTagMap == 'F') { GameMenu::bg.setTexture(tDirt); GameMenu::bg.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); GameMenu::bg_base.setTexture(tBase); GameMenu::bg_base.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); }

	if (TileTagMap == 'T') { GameMenu::bg.setTexture(tWall); GameMenu::bg.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); }

	if (TileTagMap == 'P') { GameMenu::bg.setTexture(tDirt); GameMenu::bg.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); }

	if (TileTagMap == ' ') { GameMenu::bg.setTexture(tGrass); GameMenu::bg.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize); }
}

void GameMenu::killed(char enemyType)
{
	switch (enemyType)
	{
	case 'S': //���� ���� ������
		GameMenu::strStalin = L"����\n�������!";
		GameMenu::StalinSay.setString(strStalin);
		GameMenu::money +=50;
		break;
	case 'T': //���� ���� ����
		GameMenu::strStalin = L"����\n�������!";
		GameMenu::StalinSay.setString(strStalin);
		GameMenu::money +=100;
		break;
	default:
		break;
	}
	GameMenu::killed_counter +=1;
	GameMenu::MONEY_counter.setString(std::to_string(GameMenu::money));
	GameMenu::KILLED_counter.setString(std::to_string(GameMenu::killed_counter));
}

float GameMenu::enemyOnBase(void)
{
	GameMenu::baseHP--;
	GameMenu::BASEHP_counter.setString(std::to_string(GameMenu::baseHP));
	return 0;
}

void GameMenu::drawTileMap(sf::RenderWindow &window)
{
	//��������� ��������
	//----------------------------------------------------------------------------------------
	for (int i = 0; i < GameMenu::H; i++)
		for (int j = 0; j < GameMenu::W; j++)
		{
			GameMenu::renderTileMap(TileTagMap[i][j], i, j);
			GameMenu::grid.setPosition(j*GameMenu::cellSize, i*GameMenu::cellSize);

			window.draw(GameMenu::bg);
			window.draw(GameMenu::bg_base);
			window.draw(GameMenu::grid);
		}
		//--------------------------------------------------------------------------------------
}

void GameMenu::enemiesWaves()
{
	if ( wave/1e6 >= 5 )
	{
		wave -= 5e6;
		if (waveClick == true)
		{
			if (i < 4)
			{
				pressMenuEnemy('S');
				i++;
			}
			else
			{
				pressMenuEnemy('T');
				i = 0;
			}
		}
	}
}

void GameMenu::clickingTowers(sf::RenderWindow &window)
{
	if ( sf::Mouse::isButtonPressed(sf::Mouse::Left) && !clickTower && !clickGun)
	{
		//! ����� � ����
		if (money >= 50 && sf::IntRect(sMenuTower.getPosition().x, sMenuTower.getPosition().y, cellSize, cellSize).contains(sf::Mouse::getPosition(window)))
		{
			pressMenuTower('L');
			window.setMouseCursorVisible(false);
		}
		//! ������� � ����
		if (money >= 150 && sf::IntRect(sMenuGun.getPosition().x, sMenuGun.getPosition().y, cellSize, cellSize).contains(sf::Mouse::getPosition(window)))
		{
			pressMenuTower('G');
			window.setMouseCursorVisible(false);
		}
	}
}

void GameMenu::clickingEnemy(sf::RenderWindow &window)
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && sf::IntRect(sMenuEnemy.getPosition().x, sMenuEnemy.getPosition().y, cellSize, cellSize).contains(sf::Mouse::getPosition(window)))
	{
		waveClick = true;
		wave = 5e6;
	}
}

void GameMenu::drawGame(sf::RenderWindow &window)
{
	window.draw(sMenuTower); //����� � ����
	window.draw(sMenuGun); //������� � ����
	window.draw(sMenuEnemy);//�������� ����
	window.draw(sMenuTank);//�������� ����

	window.draw(StalinCube);
	window.draw(StalinSay);

	window.draw(MONEY_TEXT);
	window.draw(MONEY_counter);

	window.draw(KILLED_TEXT);
	window.draw(KILLED_counter);

	window.draw(BASEHP_TEXT);
	window.draw(BASEHP_counter);
}