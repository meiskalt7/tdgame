#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include "Debug.h"
#include "Menu.h"
#include "MainMenu.h"
#include "GameMenu.h"
#include "Gun.h"
#include "Laser.h"
#include "Soldier.h"
#include "Tank.h"

int main()
{
	MainMenu MainMenu;

	sf::RenderWindow window(sf::VideoMode(MainMenu.windowWidth, MainMenu.windowHeight), "TDGame");
	window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(30);

	while (window.isOpen())
	{
		bool Menu_on = true;
		bool Game = true;
		int MenuNum = 0;

		MainMenu.Menu_Theme.play();

		while(Menu_on)
		{
			MainMenu.returnDefaultColors();

			MenuNum = MainMenu.getMenuPos(window);

			//�������� �������
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				switch (MenuNum)
				{
				case 1:
					Menu_on =false; //����� ���� � ������� ��� � ���� ����
					MainMenu.sClick.play();
					break;
				case 2:
					window.draw(MainMenu.sBackground);
					window.draw(MainMenu.ABOUT_TEXT);
					window.display(); while(!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape));// ����� ���� � ����������
					break;
				case 3:
					return 0;
					break;
				default:
					break;
				}
				MainMenu.sClick.play();
			}
			MainMenu.drawMenu(window);
			window.display();
		}

		MainMenu.Menu_Theme.stop();

		//LoadScreen
		MainMenu.iBackground.loadFromFile("Res/loadScreen.jpg");
		MainMenu.sBackground.setScale(0.711, 0.8); //1024/1440 768/960

		window.draw(MainMenu.sBackground);
		window.draw(MainMenu.LOADING_TEXT);
		window.display();

		//���� ����:
		Menu Menu;
		GameMenu GameMenu;

		Laser *laser_arr;
		laser_arr = new Laser[20];

		Gun *gun_arr;
		gun_arr = new Gun[20];

		Soldier *soldier_arr;
		soldier_arr = new Soldier[100];

		Tank *tank_arr;
		tank_arr = new Tank[100];

		sf::Clock clock;

		while (Game)
		{
			float time = clock.getElapsedTime().asMicroseconds();
			clock.restart();

			window.clear(sf::Color::White);
			window.draw(GameMenu.sBackground);

			GameMenu.drawTileMap(window);

			GameMenu.wave += time * 2;

			GameMenu.clickingTowers(window);

			GameMenu.clickingEnemy(window);

			GameMenu.enemiesWaves();

			laser_arr[GameMenu.num_tower-1].setLaser(GameMenu, window);

			gun_arr[GameMenu.num_gun-1].setGun(GameMenu, window);

			//! ���������
			if (GameMenu.clickEnemy) //������� �����, � ������� �����
			{
				GameMenu.EnemyV_currentFrame += soldier_arr[0].speed*time; //������ ��������� ���������� ����� �� ��������� �����
				for (int i = 0; i < GameMenu.num_enemy; i++)
				{
					soldier_arr[i].setSoldier(GameMenu, time, Menu);
				}
				if (GameMenu.EnemyV_currentFrame > GameMenu.frame * 5) {GameMenu.EnemyV_currentFrame -= GameMenu.frame * 5; }
			}

			//! ����
			if (GameMenu.clickTank) //������� �����, � ������� �����
			{
				GameMenu.TankV_currentFrame += tank_arr[0].speed*time;
				for (int i = 0; i < GameMenu.num_tank; i++)
				{
					tank_arr[i].setTank(GameMenu, time, Menu);
				}
				if (GameMenu.TankV_currentFrame > GameMenu.frame * 5) { GameMenu.TankV_currentFrame -= GameMenu.frame * 5; }
			}
			//----------------------------------------------------------------------------------------------
			//! ��������
			GameMenu.TowerV_currentFrame += 0.005*time;
			if (GameMenu.TowerV_currentFrame > GameMenu.frame) //���������� �������� �������� � ����� ���� �� ����
			{
				GameMenu.TowerV_currentFrame -= 25;
				for (int i = 0; i < GameMenu.num_tower; i++)
				{
					if (laser_arr[i].stand == false) continue;
					laser_arr[i].line.setFillColor(sf::Color::Transparent);

					int tX = laser_arr[i].sTowerBase.getPosition().x + (Menu.cellSize / 2); //�������� �������� ������, ����� �������� ������������� �� ������
					int tY = laser_arr[i].sTowerBase.getPosition().y + (Menu.cellSize / 2);

					if (!laser_arr[i].laserAttackTank(GameMenu, tank_arr, tX, tY, GameMenu.frame))
						laser_arr[i].laserAttackSoldier(GameMenu, soldier_arr, tX, tY, GameMenu.frame);
				}
			}
			//----------------------------------------------------------------------------------------------
			//! �������� c �������� ������:)
			GameMenu.GunV_currentFrame += 0.005*time;
			if (GameMenu.GunV_currentFrame > GameMenu.frame) //���������� �������� �������� � ����� ���� �� ����
			{
				GameMenu.GunV_currentFrame -= 25;
				for (int i = 0; i < GameMenu.num_gun; i++)
				{
					if (gun_arr[i].stand == false) continue;
					gun_arr[i].line.setFillColor(sf::Color::Transparent);

					int tX = gun_arr[i].sTowerBase.getPosition().x + (Menu.cellSize / 2); //�������� �������� ������, ����� �������� ���� �� ������
					int tY = gun_arr[i].sTowerBase.getPosition().y + (Menu.cellSize / 2);

					if (!gun_arr[i].gunAttakTank(GameMenu, tank_arr, tX, tY, GameMenu.frame))
						gun_arr[i].gunAttackSoldier(GameMenu, soldier_arr, tX, tY, GameMenu.frame);
				}
			}

			for (int i = 0; i < GameMenu.num_tower; i++) { window.draw(laser_arr[i].sTowerBase); window.draw(laser_arr[i].sTowerTurret); window.draw(laser_arr[i].towerRange); window.draw(laser_arr[i].line); }
			for (int i = 0; i < GameMenu.num_gun; i++) { window.draw(gun_arr[i].sTowerBase); window.draw(gun_arr[i].sTowerTurret); window.draw(gun_arr[i].towerRange); window.draw(gun_arr[i].line); }
			for (int i = 0; i < GameMenu.num_enemy; i++) { window.draw(soldier_arr[i].sEnemy); }
			for (int i = 0; i < GameMenu.num_tank; i++) { window.draw(tank_arr[i].sEnemy);}

			GameMenu.drawGame(window);

			window.display();
			if (GameMenu.baseHP <= 0) Game = false;
		}

		MainMenu.iBackground.loadFromFile("Res/GameOver.png");
		MainMenu.sBackground.setScale(1, 1);

		while(!sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.clear(sf::Color::Black); window.draw(MainMenu.sBackground); window.display();
		}

		Menu_on = true;
		MainMenu.iBackground.loadFromFile("Res/Redbanner.jpg");
	}
	return 0;
}