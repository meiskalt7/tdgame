#include "Menu.h"

Menu::Menu(void)
{
	arr_size = 0;

	for (int i = 0; i < Menu::H; i++)
		for (int j = 0; j < Menu::W; j++)
		{
			Menu::Grid[i][j].x = j*cellSize;
			Menu::Grid[i][j].y = i*cellSize;

			if (TileTagMap[i][j] == 'B') { Menu::Grid[i][j].validEnemy = Menu::Grid[i][j].validTower = false; }

			if (TileTagMap[i][j] == 'S')
			{
				Menu::enemySPosX = Menu::Grid[i][j].x;
				Menu::enemySPosY = Menu::Grid[i][j].y;
				Menu::enemy_i = i;
				Menu::enemy_j = j;
				Menu::Grid[i][j].validEnemy = Menu::Grid[i][j].validTower = false;
			}

			if (TileTagMap[i][j] == 'F') { Menu::Grid[i][j].validEnemy = true; Menu::Grid[i][j].validTower = false; Menu::finish_i = i; Menu::finish_j = j; }

			if (TileTagMap[i][j] == 'T') { Menu::Grid[i][j].validEnemy = false; Menu::Grid[i][j].validTower = true; arr_size++; }

			if (TileTagMap[i][j] == 'P') { Menu::Grid[i][j].validEnemy = true; Menu::Grid[i][j].validTower = false; }

			if (TileTagMap[i][j] == ' ') { Menu::Grid[i][j].validEnemy = false; Menu::Grid[i][j].validTower = false; }
		}
}

Menu::~Menu(void)
{
}