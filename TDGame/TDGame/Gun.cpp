#include "Gun.h"

Gun::Gun(void)
{
	damage = 5; //�����
	speed = 15; //���� ������ ����
	cost = 150; //��������� ����� ����������� �����

	tTowerBase.loadFromFile("Res/tank3_base_red.png");
	tTowerBase.setSmooth(true);

	tTowerTurret.loadFromFile("Res/tank3_turret_blue.png");
	tTowerTurret.setSmooth(true);

	sTowerBase.setTexture(tTowerBase);
	sTowerBase.setScale(cellSize / 32, cellSize / 32);
	sTowerTurret.setTexture(tTowerTurret);
	sTowerTurret.setScale(cellSize / 32, cellSize / 32);

	towerRange.setRadius(radious);
	towerRange.setFillColor(sf::Color::Transparent);
	towerRange.setOutlineColor(sf::Color::Red);
	towerRange.setOutlineThickness(1);

	sbShoot.loadFromFile("Res/gun.wav");
	sShoot.setBuffer(sbShoot);
}

Gun::~Gun(void)
{
}

bool Gun::secondClick()
{
	Gun::stand = true; /*����� ����� ������ � ����� ���� ����*/
	Gun::towerRange.setOutlineColor(sf::Color::Transparent);
	Gun::sTowerBase.setColor(sf::Color::White);
	Gun::sTowerTurret.setColor(sf::Color::White);
	return false;
}

void Gun::gunAttackSoldier(GameMenu &GameMenu, Soldier * soldier_arr, int tX, int tY, float frame)
{
	//! � �������
	for (int j = 0; j < GameMenu.num_enemy; j++)
	{
		if (!soldier_arr[j].alive()) continue;

		int eX = soldier_arr[j].sEnemy.getPosition().x;
		int eY = soldier_arr[j].sEnemy.getPosition().y;

		if ( (((eX - tX) * (eX - tX) + (eY - tY) * (eY - tY)) <= radious * radious)) //�������� �� ��������� � �������� ��������
		{
			drawLine(tX, tY, eX, eY, 'B');
			soldier_arr[j].hp -= damage/(frame/25/2);
			soldier_arr[j].slowdown();
			if (!soldier_arr[j].alive()) { GameMenu.killed('S'); }
			break; //����� �� ������� ����� �� ����, � ������ � ����, ��� ����� � "������"
		}
	}
}

bool Gun::gunAttakTank(GameMenu &GameMenu, Tank * tank_arr, int tX, int tY, float frame)
{
	//! � ���� - ����� ��������������� �������� � ���� - ��� ����
	for (int j = 0; j < GameMenu.num_tank; j++)
	{
		if (!tank_arr[j].alive()) continue;

		int eX = tank_arr[j].sEnemy.getPosition().x;
		int eY = tank_arr[j].sEnemy.getPosition().y;

		if ( (((eX - tX) * (eX - tX) + (eY - tY) * (eY - tY)) <= radious * radious)) //�������� �� ��������� � �������� ��������
		{
			drawLine(tX, tY, eX, eY, 'B');
			tank_arr[j].hp -= damage/(frame/25/2);
			tank_arr[j].slowdown();
			if (!tank_arr[j].alive())
			{
				GameMenu.killed('T');
			}
			return true; //������� ����������
		}
	}
	return false;
}

void Gun::setGun(GameMenu &GameMenu, sf::RenderWindow &window)
{
	if (GameMenu.clickGun)
	{
		moveTower(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
		//��������� ������� ��� ����������� �������
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && sTowerBase.getColor() == sf::Color::Green)
		{
			GameMenu.clickGun = secondClick();
			window.setMouseCursorVisible(true);
		}
	}
}