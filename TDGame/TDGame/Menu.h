#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Core.h"

#pragma once
//GameMenu, ���� ��������� � ����� �����
class Menu : public Core
{
	struct GridStruct
	{
		short x,y;
		bool validTower;
		bool validEnemy;
	};

public:
	Menu(void);
	~Menu(void);

	int arr_size; //������ ������� �����, ������� �� ���-�� ���� ��� �����������

	//��������� ����� ��� ���� ������
	int enemySPosX, enemySPosY; // ��������� ������� �� ��� � �����
	int enemy_i, enemy_j; // �������������� �� �����
	int finish_i, finish_j;

	GridStruct Grid[Menu::H][Menu::W]; //�����
};

//P - ����, B - ���� �����, T - ����� ��� �����, S - ��������� �����, F - ��������
static const sf::String TileTagMap[Menu::H] = {
	"BBBBBBBBB",
	"BTSTPPPTB",
	"BTPTPTPTB",
	"BTPPPTPTB",
	"BTTTTTPTB",
	"B  TPPPTB",
	"B  TPTTTB",
	"B  TFT  B",
	"BBBBBBBBB",
};
