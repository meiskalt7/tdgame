#include "Enemy.h"

Enemy::Enemy(void)
{
	x = -1; // -1 - �������� ��������� �����
	y = -1;
	hp = 500;
	speed = 0.005;
}

Enemy::~Enemy(void)
{
}

void Enemy::SpawnPos(int enemyPosX, int enemyPosY, int enemy_i, int enemy_j)
{
	//������ ��������� ���������� ����� �� ��������� �����
	if (Enemy::x == -1 && Enemy::y == -1) {
		Enemy::x = enemyPosX + cellSize / 2; // � ����� ������ ������ �����
		Enemy::y = enemyPosY + cellSize / 2;
		Enemy::sEnemy.setPosition(sf::Vector2f(Enemy::x, Enemy::y));
		Enemy::En_i = enemy_i; Enemy::En_j = enemy_j;
	}
}

void Enemy::findPath(bool Down, bool Up, bool Right, bool Left)
{
	int EnemyDvig = false;
	//"����� ����" ��� ���������� �����
	//!  �������� ������� ���������
	//�������� ���� ����
	if (Down == true && EnemyDvig == false && Enemy::En_i+1 != Enemy::post_En_i) { Enemy::y += cellSize; Enemy::post_En_i = Enemy::En_i; Enemy::post_En_j = Enemy::En_j; Enemy::En_i++; Enemy::sEnemy.setRotation(180); EnemyDvig = true; }
	//�������� ���� �����
	if (Up == true && EnemyDvig == false && Enemy::En_i-1 != Enemy::post_En_i) { Enemy::y -= cellSize; Enemy::post_En_i = Enemy::En_i; Enemy::post_En_j = Enemy::En_j; Enemy::En_i--; Enemy::sEnemy.setRotation(0); EnemyDvig = true; }
	//�������� ���� ������
	if (Right == true && EnemyDvig == false && Enemy::En_j+1 != Enemy::post_En_j) { Enemy::x += cellSize; Enemy::post_En_i = Enemy::En_i; Enemy::post_En_j = Enemy::En_j; Enemy::En_j++; Enemy::sEnemy.setRotation(90); EnemyDvig = true; }
	//�������� ���� �����
	if (Left == true && EnemyDvig == false && Enemy::En_j-1 != Enemy::post_En_j) { Enemy::x -= cellSize; Enemy::post_En_i = Enemy::En_i; Enemy::post_En_j = Enemy::En_j; Enemy::En_j--; Enemy::sEnemy.setRotation(270); EnemyDvig = true; }

	Enemy::sEnemy.setPosition(sf::Vector2f(Enemy::x, Enemy::y));
}

bool Enemy::alive()
{
	if ( Enemy::hp > 0) return true;
	else return false;
}

void Enemy::slowdown() //��������� ����� � ������ ��� � ����� ����
{
	Enemy::sEnemy.setColor(sf::Color::Blue);
	if (Enemy::speed > 0.002) Enemy::speed -= 0.001;
}