#include "Tower.h"
#include "iostream"

Tower::Tower(void)
{
	stand = false;
	strike = false;
	radious = cellSize * 2; //�� ��� ������
	iMin = 0, jMin = 0;

	sTowerTurret.setOrigin(16, 16);
	
	//������ ��������
	towerRange.setRadius(radious);
	towerRange.setFillColor(sf::Color::Transparent);
	towerRange.setOutlineColor(sf::Color::Red);
	towerRange.setOutlineThickness(1);
}

Tower::~Tower(void){}

void Tower::moveTower(int mouseX, int mouseY) // ��������� ����� - �����������
{
	//����� ��������� ����� � ������� ����
	Tower::sTowerBase.setPosition(mouseX, mouseY);
	Tower::minLen = ((Grid[0][0].x - Tower::sTowerBase.getPosition().x) * (Grid[0][0].x - Tower::sTowerBase.getPosition().x)) + ((Grid[0][0].y - Tower::sTowerBase.getPosition().y) * (Grid[0][0].y - Tower::sTowerBase.getPosition().y));

	//������� ��������� � ��������� �� ���� � ����� ������
	for (int i = 0; i < H; i++)
	{
		for (int j = 0; j < W; j++)
		{
			Tower::towerLen = ((Grid[i][j].x - Tower::sTowerBase.getPosition().x) * (Grid[i][j].x - Tower::sTowerBase.getPosition().x)) + ((Grid[i][j].y - Tower::sTowerBase.getPosition().y) * (Grid[i][j].y - Tower::sTowerBase.getPosition().y));
			if (Tower::towerLen < Tower::minLen)
			{
				Tower::minLen = ((Grid[i][j].x - Tower::sTowerBase.getPosition().x) * (Grid[i][j].x - Tower::sTowerBase.getPosition().x)) + ((Grid[i][j].y - Tower::sTowerBase.getPosition().y) * (Grid[i][j].y - Tower::sTowerBase.getPosition().y));
				Tower::iMin = i;
				Tower::jMin = j;
			}
		}
	}
	Tower::sTowerBase.setPosition(Grid[Tower::iMin][Tower::jMin].x, Grid[Tower::iMin][Tower::jMin].y); // ������������ ��������� �������
	Tower::sTowerTurret.setPosition(Grid[Tower::iMin][Tower::jMin].x + 32, Grid[Tower::iMin][Tower::jMin].y + 32);
	Tower::towerRange.setPosition(Grid[Tower::iMin][Tower::jMin].x - radious * 3 / 4, Grid[Tower::iMin][Tower::jMin].y - radious * 3 / 4);

	if (Menu::Grid[Tower::iMin][Tower::jMin].validTower)
	{
		Tower::sTowerBase.setColor(sf::Color::Green);
		Tower::sTowerTurret.setColor(sf::Color::Green);
		Tower::towerRange.setOutlineColor(sf::Color::Green);
	}
	else
	{
		Tower::sTowerBase.setColor(sf::Color::Red);
		Tower::sTowerTurret.setColor(sf::Color::Red);
		Tower::towerRange.setOutlineColor(sf::Color::Red);
	}
}

void Tower::drawLine(int tX, int tY, int eX, int eY, char Color)
{
	switch (Color)
	{
	case 'R':
		Tower::line.setFillColor(sf::Color::Red);
		break;
	case 'B':
		Tower::line.setFillColor(sf::Color::Blue);
		break;
	}
	Tower::line.setLine(sf::Vector2f(tX, tY), sf::Vector2f(eX, eY));
}