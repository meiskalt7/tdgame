#pragma once

#include "Enemy.h"
#include "GameMenu.h"

class Tank : public Enemy
{
public:
	Tank(void);
	~Tank(void);

	void setTank(GameMenu &GameMenu, float time, Menu &Menu);
};
