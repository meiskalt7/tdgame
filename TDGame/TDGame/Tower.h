#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "LineShape.h"
#include "Menu.h"

#pragma once

class Tower : public Menu
{
public:
	Tower(void);
	~Tower(void);
	int x, y; //�������
	int iMin, jMin;
	int towerLen, minLen; // minLen - ����������� ���������� �� ����� �� �����, towerLen - ���������� �� ����� �� ���� ��� ��������
	int damage, speed, radious; //����, �������� ��������, ������
	int cost; //��������� �����

	sf::CircleShape towerRange;
	sf::LineShape line;
	//���� �������� � ������
	sf::SoundBuffer sbShoot;
	sf::Sound sShoot;

	bool strike; //�������� ��� ���
	bool stand;

	void moveTower(int mouseX, int mouseY);

	sf::Texture tTowerBase;
	sf::Texture tTowerTurret;

	sf::Sprite sTowerBase;
	sf::Sprite sTowerTurret;

	void drawLine(int tX, int tY, int eX, int eY, char Color);
};
