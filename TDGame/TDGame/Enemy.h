#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Menu.h"
#include "LineShape.h"

#pragma once

class Enemy : public Menu
{
public:
	Enemy(void);
	~Enemy(void);
	float hp, speed;
	int prize; //�� �������� �����
	int En_i, En_j; // ������� �� �����
	int post_En_i, post_En_j; //������ ������� �� �����(����� �� ������������ �����)
	int x, y; //���������� �����
	void SpawnPos(int EnemyPosX, int EnemyPosY, int Enemy_i, int Enemy_j);
	void findPath(bool Down, bool Up, bool Right, bool Left);

	sf::Texture tEnemy;
	sf::Sprite sEnemy;

	bool alive(); //�������� �� �����
	void slowdown();
	void enemyClicked(float currentFrame, float time, float frame);
};
