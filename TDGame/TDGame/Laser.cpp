#include "Laser.h"

Laser::Laser(void)
{
	damage = 10;
	speed = 5;
	cost = 50; //��������� ����� �����

	tTowerBase.loadFromFile("Res/tank2_base_red.png");
	tTowerBase.setSmooth(true);

	tTowerTurret.loadFromFile("Res/tank2_turret_red.png");
	tTowerTurret.setSmooth(true);

	sTowerBase.setTexture(tTowerBase);
	sTowerBase.setScale(cellSize / 32, cellSize / 32);
	sTowerTurret.setTexture(tTowerTurret);
	sTowerTurret.setScale(cellSize / 32, cellSize / 32);

	//������ ��������
	towerRange.setRadius(radious);
	towerRange.setFillColor(sf::Color::Transparent);
	towerRange.setOutlineColor(sf::Color::Red);
	towerRange.setOutlineThickness(1);

	sbShoot.loadFromFile("Res/laser.wav");
	sShoot.setBuffer(sbShoot);
}

Laser::~Laser(void)
{
}

bool Laser::secondClick()
{
	Laser::stand = true; /*����� ����� ������ � ����� ���� ����*/
	Laser::towerRange.setOutlineColor(sf::Color::Transparent);
	Laser::sTowerBase.setColor(sf::Color::White);
	Laser::sTowerTurret.setColor(sf::Color::White);
	return false;
}

void Laser::laserAttackSoldier(GameMenu &GameMenu, Soldier * soldier_arr, int tX, int tY, float frame)
{
	//! � �������
	for (int j = 0; j < GameMenu.num_enemy; j++)
	{
		if (!soldier_arr[j].alive()) continue;

		int eX = soldier_arr[j].sEnemy.getPosition().x;
		int eY = soldier_arr[j].sEnemy.getPosition().y;

		if ( (((eX - tX) * (eX - tX) + (eY - tY) * (eY - tY)) <= radious * radious)) //�������� �� ��������� � �������� ��������
		{
			drawLine(tX, tY, eX, eY, 'R');
			soldier_arr[j].hp -= damage/(frame/25/2);
			if (!soldier_arr[j].alive()) { GameMenu.killed('S'); }
			break; //����� �� ������� ����� �� ����, � ������ � ����, ��� ����� � "������"
		}
	}
}

bool Laser::laserAttackTank(GameMenu &GameMenu, Tank * tank_arr, int tX, int tY, float frame)
{
	//! � ����
	for (int j = 0; j < GameMenu.num_tank; j++)
	{
		if (!tank_arr[j].alive()) continue;

		int eX = tank_arr[j].sEnemy.getPosition().x;
		int eY = tank_arr[j].sEnemy.getPosition().y;

		if ( (((eX - tX) * (eX - tX) + (eY - tY) * (eY - tY)) <= radious * radious)) //�������� �� ��������� � �������� ��������
		{
			drawLine(tX, tY, eX, eY, 'R');
			tank_arr[j].hp -= damage/(frame/25/2);
			if (!tank_arr[j].alive()) { GameMenu.killed('T'); }
			return true; //������� ����������
		}
	}
	return false;
}

void Laser::setLaser(GameMenu &GameMenu, sf::RenderWindow &window)
{
	if (GameMenu.clickTower)
	{
		moveTower(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y);
		//��������� ������� ��� ����������� �������, !���� ��� �� ���� ����
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && sTowerBase.getColor() == sf::Color::Green)
		{
			GameMenu.clickTower = secondClick();
			window.setMouseCursorVisible(true);
		}
	}
}