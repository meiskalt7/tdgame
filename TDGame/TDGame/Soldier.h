#pragma once
#include "Enemy.h"
#include "GameMenu.h"

class Soldier :
	public Enemy
{
public:
	Soldier(void);
	~Soldier(void);
	void setSoldier(GameMenu &GameMenu, float time, Menu &Menu);
};

