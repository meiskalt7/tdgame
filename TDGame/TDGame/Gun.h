#pragma once

#include "Tower.h"
#include "GameMenu.h"
#include "Soldier.h"
#include "Tank.h"

class Gun : public Tower
{
public:
	Gun(void);
	~Gun(void);
	bool secondClick();
	void gunAttackSoldier(GameMenu &GameMenu, Soldier * soldier_arr, int tX, int tY, float frame);
	bool gunAttakTank(GameMenu &GameMenu, Tank * tank_arr, int tX, int tY, float frame);
	void setGun(GameMenu &GameMenu, sf::RenderWindow &window);
};
